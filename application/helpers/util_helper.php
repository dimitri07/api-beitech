<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    

function sendBasicOutput(int $code, $data, string $description = '') :array
{	
    return [
        'code'			=> $code,
        'data'			=> $data,
        'description'	=> $description
    ];
}
    