<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization, X-JP-Access-Token, Access-Control-Allow-Headers, X-JP-Auth-Token");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
}

class Customer extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('CustomerModel', 'customer');
    }

    public function getAllCustomer()
    {
        echo json_encode(
            sendBasicOutput(200, $this->customer->getAll(), 'Operación exitosa')
        );
    }
}