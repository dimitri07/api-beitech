<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization, X-JP-Access-Token, Access-Control-Allow-Headers, X-JP-Auth-Token");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
}

class Order extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('OrderModel', 'order');
        $this->load->model('CustomerModel', 'customer');
        $this->load->model('ProductModel', 'product');
    }

    public function createOrder()
    {
        $customerId = (int) $this->input->post('customer_id', 0);
        if (!$this->customer->existsInDb(['customer_id' => $customerId]))
        {
            echo json_encode(sendBasicOutput(400, [], 'Identificador de cliente no existe'));
            exit();
        }
        echo json_encode($this->order->createOrder(
            $customerId,
            json_decode($this->input->post('products')),
            $this->input->post('delivery_address')
        ));
    }

    public function listOrders()
    {
        $customerId =  (int) $this->input->post('customer_id', 0);
        if ($customerId !== 0 &&  !$this->customer->existsInDb(['customer_id' => $customerId]))
        {
            echo json_encode(sendBasicOutput(400, [], 'Identificador de cliente no existe'));
            exit();
        }
        echo json_encode($this->order->listOrders(
            $customerId,
            $this->input->post('startDate'),
            $this->input->post('endDate')
        ));
    }
}