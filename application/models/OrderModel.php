<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once (dirname(__FILE__).'/MatrixModel.php');

class OrderModel extends MatrixModel
{
    protected $table = 'order';
    const MAX_AMOUNT_PRODUCTS_BY_ORDER = 5;

    function __construct()
    {
       parent::__construct();
       $this->load->model('CustomerProductModel', 'customerProduct');
       $this->load->model('ProductModel', 'product');
       $this->load->model('OrderDetailModel', 'orderDetail');
    }

    public function createOrder(int $customerId, array $products, string $address): array
    {
        $totalQtyProducts = 0;
        // Validate customer can buy products
        foreach ($products as $key => $product) 
        {
            if (!$this->customerProduct->canCustomerBuyProduct($customerId, $product->product_id)) 
            {
                $productInfo = $this->product->getByCriteria(['product_id' => (int) $product->product_id], true);
                return sendBasicOutput(
                    400, 
                    [],
                    'Usuario no puede realizar la compra del producto' . $productInfo->name . ' con ID: ' . $productInfo->product_id  
                );
            }

            $totalQtyProducts += (int) $product->quantity;
            if ($totalQtyProducts > self::MAX_AMOUNT_PRODUCTS_BY_ORDER)
                return sendBasicOutput(400, [], 'Ha excedido la cantidad máxima de productos permitida por orden'); 
        }
        $totalPrice = 0.00;
        $orderId = $this->save([
            'customer_id'       => $customerId,
            'creation_date'     => date('Y-m-d'),
            'delivery_address'  => $address,
            'total'             => $totalPrice
        ]);
        foreach ($products as $key => $product) 
        {
            $productInfo = $this->product->getByCriteria(['product_id' => (int) $product->product_id], true);
            $this->orderDetail->save([
                'order_id'      => $orderId,
                'product_id'    => $productInfo->product_id,
                'product_description'  => $productInfo->product_description,
                'price'     => $productInfo->price,
                'quantity'  => $product->quantity
            ]);
            $totalPrice = $totalPrice + ($productInfo->price * $product->quantity);
        }
        $this->update(['total' => $totalPrice], 'order_id', $orderId);
        return sendBasicOutput(200, [], 'Order creada correctamente');
    }

    public function listOrders(int $customerId, string $startDate, string $endDate): array
    {
        $criteria = [
            'creation_date >='  => $startDate,
            'creation_date <='        => $endDate
        ]; 
        if ($customerId !== 0)
            $criteria['customer_id'] = $customerId; 
        $orders = $this->getByCriteria(
            $criteria,
            false,
            ['creation_date'    => 'DESC']
        );
        foreach ($orders as $key => $order)
        {
            $this->db->select('*');
            $this->db->from('order_detail');
            $this->db->where('order_detail.order_id', $order->order_id);
            $this->db->join('product', 'order_detail.product_id = product.product_id');
            $query = $this->db->get();
            $order->products =  $query->result();
        }
        return sendBasicOutput(200, $orders, 'Operación exitosa');
    }
}