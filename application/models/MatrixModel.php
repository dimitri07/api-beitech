<?php

abstract class MatrixModel extends CI_Model
{	
    protected $table = '';
	private $db_engine = 'mysql'; 
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getByCriteria(
		array $criteria,
		bool $oneResult = FALSE,
		array $orderBy = [],
		int $limitResults = 0,
		int $offset = 0
	)
	{
		if (!empty($orderBy)) 
		{
			foreach ($orderBy as $key => $order) 			
				$this->db->order_by($key, $order);
		}

		if ($limitResults > 0) 
			$this->db->limit($limitResults, $offset);

		if (empty($criteria)) 
			return ($this->db->get($this->table))->result();
			 
		$query = $this->db->get_where($this->table, $criteria);
		return  ($oneResult) ? $query->row() :  $query->result();
	}
	

	public function getBySimilarCriteria(array $criteria) :array
	{
		$this->db->or_like($criteria);
		return ($this->db->get($this->table))->result();
	}

	public function getAll() :array
	{
		return ($this->db->get($this->table))->result();
	}

	public function countResults(array $criteria = []) :int
	{
		if (empty($criteria)) 
			return $this->db->count_all_results($this->table);
		$this->db->where($criteria);
		$this->db->from($this->table);
		return $this->db->count_all_results();		
	}

	/*
	*  Equivalent to insert data into a table
	*   return: id with which the data was saved
	*/
	public function save(array $data) :int
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update(array $data, string $where, $equalsTo)
	{
		$this->db->where($where, $equalsTo);
		$this->db->update($this->table, $data);
	}

	public function existsInDb(array $criteria) :bool
	{
		return  (NULL !== (($this->db->get_where($this->table, $criteria))->row()));	
	}

	public function remove(array $criteria) :bool
	{
		$this->db->delete($this->table, $criteria);
		return ($this->db->affected_rows() > 0);
	}

}