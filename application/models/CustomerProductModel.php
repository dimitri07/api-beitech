<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once (dirname(__FILE__).'/MatrixModel.php');

class CustomerProductModel extends MatrixModel
{
   protected $table = 'customer_product';

    function __construct()
    {
       parent::__construct();
    }

    public function canCustomerBuyProduct(int $customerId, int $productId): bool
    {
        return $this->existsInDb(['customer_id' => $customerId, 'product_id' => $productId]);
    }
}