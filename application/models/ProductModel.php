<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once (dirname(__FILE__).'/MatrixModel.php');

class ProductModel extends MatrixModel
{
    protected $table = 'product';

    function __construct()
    {
       parent::__construct();
    }
}