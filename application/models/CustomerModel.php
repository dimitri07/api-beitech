<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once (dirname(__FILE__).'/MatrixModel.php');

class CustomerModel extends MatrixModel
{
   protected $table = 'customer';

    function __construct()
    {
       parent::__construct();
    }
}