<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once (dirname(__FILE__).'/MatrixModel.php');

class OrderDetailModel extends MatrixModel
{
   protected $table = 'order_detail';

    function __construct()
    {
       parent::__construct();
    }
}